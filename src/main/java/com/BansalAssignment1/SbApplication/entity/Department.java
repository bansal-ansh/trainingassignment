package com.BansalAssignment1.SbApplication.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
//@AttributeOverrides({
//        @AttributeOverride(
//                name = "departmentNo",
//                column = @Column(name = "department_no")
//        ),
//        @AttributeOverride(
//                name = "departmentFirstName",
//                column = @Column(name = "department_first_name")
//        ),
//        @AttributeOverride(
//                name = "departmentAddress",
//                column = @Column(name = "department_address")
//        ),
//        @AttributeOverride(
//                name = "departmentCode",
//                column = @Column(name = "department_code")
//        )
//})
public class Department {

    @Id
  //  @GeneratedValue(strategy = GenerationType.AUTO)
    private long departmentNo;
    private String departmentName;
    private String departmentAddress;
    private String departmentCode;

    @ManyToMany(
           mappedBy = "departments"
          // cascade = CascadeType.ALL
    )
//    @JoinTable(
//            name = "department_employee_map",
//            joinColumns = @JoinColumn(
//                    name = "department_no",
//                    referencedColumnName = "departmentNo"
//            ),
//            inverseJoinColumns = @JoinColumn(
//                    name = "employee_no",
//                    referencedColumnName = "employeeNo"
//            )
//    )
    @JsonIgnore
    public List<Employee> employees;


}
