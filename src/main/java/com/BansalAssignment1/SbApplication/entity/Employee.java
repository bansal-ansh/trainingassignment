package com.BansalAssignment1.SbApplication.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
//@Table(name = "tbl_employee",
//        uniqueConstraints = @UniqueConstraint(
//                name = "employeeFirstName",
//                columnNames = "employee_name"
//        )
//)
public class Employee {
    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    private long employeeNo;

    @Column(name = "employee_name",
            nullable = false
    )
    private String employeeFirstName;
    private String employeeLastName;
    private String employeeAddress;
    private String employeeCode;

    @ManyToMany(
            cascade = CascadeType.ALL
    )
    @JoinTable(
            name = "employee_department_map",
            joinColumns = @JoinColumn(
                    name = "employee_no",
                    referencedColumnName = "employeeNo"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "department_no",
                    referencedColumnName = "departmentNo"
            )
    )
    public List<Department> departments;

    public void addDepartments(Department department){
        if(departments == null) departments=new ArrayList<>();
        departments.add(department);
    }
}
