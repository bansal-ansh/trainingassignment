package com.BansalAssignment1.SbApplication.service;

import com.BansalAssignment1.SbApplication.entity.Employee;

import java.util.List;

public interface EmployeeService {
    public Employee saveEmployee(Employee employee);

    public List<Employee> fetchEmployeeList();

    public Employee fetchEmployeeById(Long employeeId);

    public void deleteEmployeeById(Long employeeId);

    public Employee updateEmployee(Long employeeId, Employee employee);
}
