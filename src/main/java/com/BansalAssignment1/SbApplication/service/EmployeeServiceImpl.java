package com.BansalAssignment1.SbApplication.service;

import com.BansalAssignment1.SbApplication.entity.Department;
import com.BansalAssignment1.SbApplication.entity.Employee;
import com.BansalAssignment1.SbApplication.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;
    @Override
    public Employee saveEmployee(Employee employee) {
        Optional<Employee> employee1=employeeRepository.findById(employee.getEmployeeNo());
        if(employee1.isPresent()){
            Employee employee2=employee1.get();
            employee2.addDepartments(employee.getDepartments().get(0));
            return employeeRepository.save(employee2);
        }
        return employeeRepository.save(employee);
    }

    @Override
    public List<Employee> fetchEmployeeList() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee fetchEmployeeById(Long employeeId) {
        return employeeRepository.findById(employeeId).get();
    }

    @Override
    public void deleteEmployeeById(Long employeeId) {
          employeeRepository.deleteById(employeeId);
    }

    @Override
    public Employee updateEmployee(Long employeeId, Employee employee) {
        Employee empDB =employeeRepository.findById(employeeId).get();
        if(Objects.nonNull(employee.getEmployeeFirstName()) && !"".equalsIgnoreCase(employee.getEmployeeFirstName())){
            empDB.setEmployeeFirstName(employee.getEmployeeFirstName());
        }
        if(Objects.nonNull(employee.getEmployeeLastName()) && !"".equalsIgnoreCase(employee.getEmployeeLastName())){
            empDB.setEmployeeLastName(employee.getEmployeeLastName());
        }
        if(Objects.nonNull(employee.getEmployeeCode()) && !"".equalsIgnoreCase(employee.getEmployeeCode())){
            empDB.setEmployeeCode(employee.getEmployeeCode());
        }
        if(Objects.nonNull(employee.getEmployeeAddress()) && !"".equalsIgnoreCase(employee.getEmployeeAddress())){
            empDB.setEmployeeAddress(employee.getEmployeeAddress());
        }
        return employeeRepository.save(empDB);
    }
}
