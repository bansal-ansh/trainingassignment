package com.BansalAssignment1.SbApplication.repository;

import com.BansalAssignment1.SbApplication.entity.Department;
import com.BansalAssignment1.SbApplication.entity.Employee;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class EmployeeRepositoryTest {

      @Autowired
      private EmployeeRepository employeeRepository;

      @Test
      public void saveCourseWithEmployeeAndDeparment(){

            Department department=Department
                    .builder()
                    .departmentNo(16L)
                    .departmentName("CS")
                    .departmentAddress("Gurgao")
                    .departmentCode("unthinkable")
                    .build();
          Employee employee=Employee
                  .builder()
                  .employeeNo(15L)
                  .employeeFirstName("Prabhat")
                  .employeeLastName("Tiwari")
                  .employeeAddress("Lucknow")
                  .employeeCode("APS-3876")
                  .build();
         employee.addDepartments(department);

          employeeRepository.save(employee);

      }
}