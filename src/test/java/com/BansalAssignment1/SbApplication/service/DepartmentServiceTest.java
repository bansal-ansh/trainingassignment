package com.BansalAssignment1.SbApplication.service;

import com.BansalAssignment1.SbApplication.entity.Department;
import com.BansalAssignment1.SbApplication.repository.DepartmentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class DepartmentServiceTest {

    @Autowired
    private DepartmentService departmentService;

    @MockBean
    private DepartmentRepository departmentRepository;

    @BeforeEach
    void setUp() {
        Department department =Department.builder().departmentName("IT")
                .departmentAddress("Agra")
                .departmentCode("APS-305")
                .departmentNo(2L)
                .build();

        Mockito.when(departmentRepository.findById(2L)).thenReturn(Optional.ofNullable(department));

    }

        @Test
        @DisplayName("Get Data based on Valid Department No")
        public void whenValidDepartmentId_thenDepartmentShouldFound(){
            long departmentId = 2;
            Department found=departmentService.fetchDepartmentById(departmentId);

            assertEquals(departmentId,found.getDepartmentNo());
        }

}